﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

namespace AdornerTestDemo
{
	public class AdornerExtensions : DependencyObject
	{
		public static bool GetShowAdorner(DependencyObject obj)
		{
			return (bool)obj.GetValue(ShowAdornerProperty);
		}
		public static void SetShowAdorner(DependencyObject obj, bool value)
		{
			obj.SetValue(ShowAdornerProperty, value);
		}
		public static readonly DependencyProperty ShowAdornerProperty =
			DependencyProperty.RegisterAttached("ShowAdorner", typeof(bool), typeof(AdornerExtensions), new PropertyMetadata(false, Method));

		private static void Method(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var ele = d as Visual;

			//var t1 = VisualTreeHelper.GetParent(ele);
			//var t2 = VisualTreeHelper.GetParent(t1);
			//var t3 = VisualTreeHelper.GetParent(t2);

			var adolay = AdornerLayer.GetAdornerLayer(ele);
			if (adolay != null)
			{
				var ados = adolay.GetAdorners(ele as UIElement);
				if (ados == null)
				{
					adolay.Add(new KDAdorner(ele as UIElement));
				}
				ados = adolay.GetAdorners(ele as UIElement);
				if (ados != null && ados.Count() != 0)
				{
					var ado = ados.FirstOrDefault() as KDAdorner;

					if ((bool) e.NewValue)
					{
						ado.ShowImage();
					}
					else
					{
						ado.HideImage();
					}
				}
			}
			else
			{
				(ele as FrameworkElement).Loaded += AdornerExtensions_Loaded;

			}
		}

		private static void AdornerExtensions_Loaded(object sender, RoutedEventArgs e)
		{
			var control = sender as FrameworkElement;
			if (control != null)
			{
				control.Loaded -= AdornerExtensions_Loaded;

				var adolay = AdornerLayer.GetAdornerLayer(control);
				if (adolay != null)
				{
					var ados = adolay.GetAdorners(control);
					if (ados == null)
					{
						adolay.Add(new KDAdorner(control));
					}
					ados = adolay.GetAdorners(control);
					if (ados != null && ados.Count() != 0)
					{
						var ado = ados.FirstOrDefault() as KDAdorner;

						if (GetShowAdorner(control))
						{
							ado.ShowImage();
						}
						else
						{
							ado.HideImage();
						}
					}
				}
			}
		}



		public static UIElement GetAdorner(DependencyObject obj)
		{
			return (UIElement)obj.GetValue(AdornerProperty);
		}

		public static void SetAdorner(DependencyObject obj, UIElement value)
		{
			obj.SetValue(AdornerProperty, value);
		}

		public static readonly DependencyProperty AdornerProperty =
			DependencyProperty.RegisterAttached("Adorner", typeof(UIElement), typeof(AdornerExtensions), new UIPropertyMetadata(OnAdornerChanged));

		private static void OnAdornerChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var control = d as FrameworkElement;
			if (control != null)
			{
				var adornerLayer = AdornerLayer.GetAdornerLayer(control);
				if (adornerLayer == null)
				{
					control.Loaded += OnControlLoaded;
				}
				else
				{
					var adorner = e.NewValue as UIElement;
					if (adorner != null)
					{
						var adorners = adornerLayer.GetAdorners(control);
						if (adorners == null || adorners.Length == 0)
						{
							adornerLayer.Add(new CustomAdorner(control, adorner));
						}
					}
				}
			}
		}

		private static void OnControlLoaded(object sender, RoutedEventArgs e)
		{
			var control = sender as FrameworkElement;
			if (control != null)
			{
				var adornerLayer = AdornerLayer.GetAdornerLayer(control);
				if (adornerLayer != null)
				{
					var adorner = GetAdorner(control);
					if (adorner != null)
					{
						control.Loaded -= OnControlLoaded;
						var adorners = adornerLayer.GetAdorners(control);
						if (adorners == null || adorners.Length == 0)
						{
							adornerLayer.Add(new CustomAdorner(control, adorner));
						}
					}
				}
			}
		}
	}
}
