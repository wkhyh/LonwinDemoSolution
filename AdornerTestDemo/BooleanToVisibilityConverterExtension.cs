﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;

namespace AdornerTestDemo
{
    [MarkupExtensionReturnType(typeof(IValueConverter))]
	public class BooleanToVisibilityConverterExtension : MarkupExtension, IValueConverter
	{
        private static IValueConverter _converter;

		public override object ProvideValue(IServiceProvider serviceProvider)
		{
			return _converter ?? (_converter = new BooleanToVisibilityConverterExtension());
		}

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return (bool)value ? Visibility.Visible : Visibility.Collapsed;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return (Visibility)value == Visibility.Visible;
		}
	}
}
