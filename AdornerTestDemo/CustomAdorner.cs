﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;

namespace AdornerTestDemo
{
	public class CustomAdorner : Adorner
	{
		private VisualCollection _visuals;
		private UIElement _adorner;

		public CustomAdorner(UIElement adornedElement, UIElement adorner) : base(adornedElement)
		{
			_visuals = new VisualCollection(this);
			_adorner = adorner;
			_visuals.Add(_adorner);
		}

		protected override int VisualChildrenCount { get { return _visuals.Count; } }

		protected override Visual GetVisualChild(int index) { return _visuals[index]; }

		protected override Size ArrangeOverride(Size finalSize)
		{
			_adorner.Arrange(new Rect(finalSize));
			return base.ArrangeOverride(finalSize);
		}
	}
}
