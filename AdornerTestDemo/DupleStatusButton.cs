﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Documents;

namespace AdornerTestDemo
{
	public class DupleStatusButton : ToggleButton
	{
		public object ActivatedContent
		{
			get { return (object)GetValue(ActivatedContentProperty); }
			set { SetValue(ActivatedContentProperty, value); }
		}

		public static readonly DependencyProperty ActivatedContentProperty =
			DependencyProperty.Register("ActivatedContent", typeof(object), typeof(DupleStatusButton));

		private Adorner _activatedAdorner;
		private ToggleButton _activatedToggleButton;

		public override void OnApplyTemplate()
		{
			base.OnApplyTemplate();

			//var style = new Style(typeof(ToggleButton));
			_activatedToggleButton = new DupleStatusButton
			{
				Style = Style,
				Content = ActivatedContent
			};
			_activatedToggleButton.Unchecked += OnActivatedToggleButtonUnchecked;
			_activatedAdorner = new CustomAdorner(this, _activatedToggleButton);
		}

		private void OnActivatedToggleButtonUnchecked(object sender, RoutedEventArgs e)
		{
			IsChecked = false;
		}

		protected override void OnChecked(RoutedEventArgs e)
		{
			base.OnChecked(e);

			var adornerLayer = AdornerLayer.GetAdornerLayer(this);
			if (adornerLayer != null)
			{
				var adorners = adornerLayer.GetAdorners(this);
				if (adorners == null || adorners.Length == 0)
				{
					_activatedToggleButton.IsChecked = true;
					adornerLayer.Add(_activatedAdorner);
				}
			}
		}

		protected override void OnUnchecked(RoutedEventArgs e)
		{
			base.OnUnchecked(e);

			var adornerLayer = AdornerLayer.GetAdornerLayer(this);
			var adorners = adornerLayer?.GetAdorners(this);
			if (adorners != null && adorners.Length != 0)
			{
				adornerLayer.Remove(_activatedAdorner);
			}
		}
	}
}
