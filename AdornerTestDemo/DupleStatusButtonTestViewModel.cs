﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight;

namespace AdornerTestDemo
{
	public class DupleStatusButtonTestViewModel : ViewModelBase
	{
		private bool _flag;

		public DupleStatusButtonTestViewModel()
		{

		}

		public bool Flag
		{
			get { return _flag; }
			set
			{
				_flag = value; 
				RaisePropertyChanged();
			}
		}
	}
}
