﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

namespace AdornerTestDemo
{
	public class KDAdorner : Adorner
	{
		private VisualCollection _visuals;
		private Canvas _grid;
		private Border _br;
		public KDAdorner(UIElement ele)
			: base(ele)
		{
			_visuals = new VisualCollection(this);
			_br = new Border();
			_br.CornerRadius = new CornerRadius(50);
			_br.Background = Brushes.Red;
			TextBlock _txt = new TextBlock();
			_txt.Text = "4";
			_br.Width = _br.Height = 20;
			_txt.Foreground = Brushes.White;
			_txt.TextAlignment = TextAlignment.Center;
			_br.Child = _txt;
			//_br.Child = new Button { Content = "TestButton", Width = 50, Height = 50 };
			_grid = new Canvas();
			//_grid.Background = new SolidColorBrush(Color.FromArgb(10, 10, 10, 10));
			_grid.Children.Add(_br);
			_visuals.Add(_grid);
		}
		protected override Visual GetVisualChild(int index)
		{
			return _visuals[index];
		}
		public void ShowImage()
		{
			_grid.Visibility = System.Windows.Visibility.Visible;
		}
		public void HideImage()
		{
			_grid.Visibility = System.Windows.Visibility.Hidden;
		}
		protected override Size ArrangeOverride(Size finalSize)
		{
			_grid.Arrange(new Rect(finalSize));
			//_br.Arrange(new Rect(finalSize));
			_br.Margin = new Thickness(finalSize.Width - 10, -10, 0, 0);
			return base.ArrangeOverride(finalSize);
		}

		protected override Size MeasureOverride(Size constraint)
		{
			return base.MeasureOverride(constraint);
		}

		protected override int VisualChildrenCount
		{
			get
			{
				return _visuals.Count;
			}
		}
	}
}
