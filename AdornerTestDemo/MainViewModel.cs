﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace AdornerTestDemo
{
	public class MainViewModel : ViewModelBase
	{
		private string _text;

		public string Text
		{
			get { return _text; }
			set
			{
				_text = value; 
				RaisePropertyChanged();
			}
		}

		public ICommand TestCommand { get; }

		public MainViewModel()
		{
			_text = "";

			TestCommand = new RelayCommand(OnTestCommand);
		}

		private void OnTestCommand()
		{
			Text += "1";
		}
	}
}
