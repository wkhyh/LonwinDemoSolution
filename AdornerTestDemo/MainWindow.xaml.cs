﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AdornerTestDemo
{
	/// <summary>
	/// MainWindow.xaml 的交互逻辑
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			var win = new DupleStatusButtonTestWindow();
			win.ShowDialog();
			Close();


			InitializeComponent();
			DataContext = new MainViewModel();
			var adolay = AdornerLayer.GetAdornerLayer(Button1);
		}

		public override void OnApplyTemplate()
		{
			base.OnApplyTemplate();
			var adolay = AdornerLayer.GetAdornerLayer(Button1);
		}

		private void Button1_OnClick(object sender, RoutedEventArgs e)
		{
			var adolay = AdornerLayer.GetAdornerLayer(Button1);
		}

		private void Button1_OnLoaded(object sender, RoutedEventArgs e)
		{
			var adolay = AdornerLayer.GetAdornerLayer(Button1);
		}
	}
}
