﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Documents;

namespace AdornerTestDemo
{
	public class TestButton : Button
	{
		public override void OnApplyTemplate()
		{
			base.OnApplyTemplate();

			var adolay = AdornerLayer.GetAdornerLayer(this);

			if (adolay != null)
			{
				var ados = adolay.GetAdorners(this);
				if (ados == null)
				{
					adolay.Add(new KDAdorner(this));
				}
				ados = adolay.GetAdorners(this);
				if (ados != null && ados.Length != 0)
				{
					var ado = ados.FirstOrDefault() as KDAdorner;
					ado.ShowImage();
				}
			}
		}
	}
}
