﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BackgroundScrrenshotDemo
{
	/// <summary>
	/// MainWindow.xaml 的交互逻辑
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();
		}

		public static BitmapSource ConvertUiElementToBitmapSource(UIElement element, double width, double height)
		{
			var visual = new DrawingVisual();
			using (var drawingContext = visual.RenderOpen())
			{
				var brush = new VisualBrush(element);
				drawingContext.DrawRectangle(brush, null, new Rect(new Point(0, 0), new Point(width, height)));
			}

			var target = new RenderTargetBitmap((int)width, (int)height, 96, 96, PixelFormats.Pbgra32);
			target.Render(visual);
			return target;
		}

		public static void Save(BitmapSource bitmapSource, string path)
		{
			BitmapEncoder encoder = new PngBitmapEncoder();
			encoder.Frames.Add(BitmapFrame.Create(bitmapSource));

			using (var stream = new FileStream(path, FileMode.Create))
			{
				encoder.Save(stream);
			}
		}

		private async void Button_Click(object sender, RoutedEventArgs e)
		{
			//var grid = new Grid { Background = Brushes.LightBlue, Width = 100, Height = 100 };
			//var textBox = new TextBox { Height = 23, Width = 100, Text = "abc" };

			//grid.Children.Add(textBox);

			//textBox.Measure(new Size((int)textBox.Width, (int)textBox.Height));
			//textBox.Arrange(new Rect(new Size((int)textBox.Width, (int)textBox.Height)));

			//var temp = ConvertUiElementToBitmapSource(grid, grid.Width, grid.Height);
			//Save(temp, @"F:\Images\grid1.png");

			//temp = ConvertUiElementToBitmapSource(textBox, textBox.Width, textBox.Height);
			//Save(temp, @"F:\Images\textBox1.png");


			//Grid1.Children.Add(grid);

			//temp = ConvertUiElementToBitmapSource(grid, grid.Width, grid.Height);
			//Save(temp, @"F:\Images\grid2.png");
			//temp = ConvertUiElementToBitmapSource(textBox, textBox.Width, textBox.Height);
			//Save(temp, @"F:\Images\textBox2.png");


			//var thread = new Thread(Test);
			//thread.SetApartmentState(ApartmentState.STA);
			//thread.Start();

			//var scheduler = TaskScheduler.FromCurrentSynchronizationContext();

			//Task.Factory.StartNew(Test, CancellationToken.None, TaskCreationOptions.None, scheduler);


			var str = await StartStaTask(Test1);
		}

		private void Test()
		{
			var grid = new Grid { Background = Brushes.LightBlue, Width = 100, Height = 100 };
			var textBox = new TextBox { Height = 23, Width = 100, Text = "abc" };

			grid.Children.Add(textBox);

			grid.Measure(new Size((int)grid.Width, (int)grid.Height));
			grid.Arrange(new Rect(new Size((int)grid.Width, (int)grid.Height)));
			//textBox.Measure(new Size((int)textBox.Width, (int)textBox.Height));
			//textBox.Arrange(new Rect(new Size((int)textBox.Width, (int)textBox.Height)));

			var temp = ConvertUiElementToBitmapSource(grid, grid.Width, grid.Height);
			Save(temp, @"F:\Images\grid1.png");

			//temp = ConvertUiElementToBitmapSource(textBox, textBox.Width, textBox.Height);
			//Save(temp, @"F:\Images\textBox1.png");

			//temp = ConvertUiElementToBitmapSource(Button1, Button1.ActualWidth, Button1.ActualHeight);
			//Save(temp, @"F:\Images\button1.png");


			//Thread.Sleep(10000);

			//Dispatcher.Invoke(new Action(() => Grid1.Children.Add(grid)));

			BitmapEncoder encoder = new PngBitmapEncoder();
			encoder.Frames.Add(BitmapFrame.Create(temp));

			//using (var stream = new MemoryStream())
			//{
			//}

			var stream = new MemoryStream();
			encoder.Save(stream);

			Dispatcher.Invoke(new Action(() =>
			{
				try
				{


					var imageToDisplay = new BitmapImage();
					imageToDisplay.BeginInit();
					imageToDisplay.StreamSource = stream;
					imageToDisplay.EndInit();

					//BitmapFrame frame = BitmapFrame.Create(temp);
					Image1.Source = imageToDisplay;
				}
				catch (Exception e)
				{
					Console.WriteLine(e);
					throw;
				}
			}
			));
		}

		private string Test1()
		{
			Test();
			return "abc";
		}


		private Task StartStaTask(Action action)
		{
			TaskCompletionSource<object> source = new TaskCompletionSource<object>();
			Thread thread = new Thread(() =>
			{
				try
				{
					action();
					source.SetResult(null);
				}
				catch (Exception ex)
				{
					source.SetException(ex);
				}
			});
			thread.SetApartmentState(ApartmentState.STA);
			thread.Start();
			return source.Task;
		}

		private Task<TResult> StartStaTask<TResult>(Func<TResult> function)
		{
			TaskCompletionSource<TResult> source = new TaskCompletionSource<TResult>();
			Thread thread = new Thread(() =>
			{
				try
				{
					source.SetResult(function());
				}
				catch (Exception ex)
				{
					source.SetException(ex);
				}
			});
			thread.SetApartmentState(ApartmentState.STA);
			thread.Start();
			return source.Task;
		}
	}
}
