﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Markup;

namespace BindingProxyDemo
{
	public class BindingCollection : ObservableCollection<BindingBase>
	{
		//public void Bind()
		//{
			
		//}

		//public void UnBind()
		//{
		//	//MultiBinding
		//}

		private FrameworkElement _proxy;

		public BindingCollection()
		{
			_proxy = new FrameworkElement();
		}

		protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
		{
			base.OnCollectionChanged(e);

			switch (e.Action)
			{
				case NotifyCollectionChangedAction.Add:
					foreach (Binding item in e.NewItems)
					{
						var temp = item.Source;
						_proxy.SetBinding(FrameworkElement.TagProperty, item);
					}
					break;
				//case NotifyCollectionChangedAction.Remove:
				//	foreach (BindingBase item in e.NewItems)
				//	{
				//		_proxy.SetBinding(FrameworkElement.TagProperty, item);
				//	}
				//	break;
			}
		}
	}
}
