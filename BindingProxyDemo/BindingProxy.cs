﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BindingProxyDemo
{
	public static class BindingProxy
	{
		public static BindingCollection GetBindings(DependencyObject obj)
		{
			var collection = obj.GetValue(BindingsProperty) as BindingCollection;
			if (collection == null)
			{
				collection = new BindingCollection();
				obj.SetValue(BindingsProperty, collection);
			}
			return collection;
		}

		public static readonly DependencyProperty BindingsProperty = DependencyProperty.RegisterAttached("ShadowBindings",
			typeof(BindingCollection), typeof(BindingProxy), new UIPropertyMetadata());

		//private static void OnBindingsChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		//{
		//	var oldValue = e.OldValue as BindingCollection;
		//	var newValue = e.NewValue as BindingCollection;
		//	if (oldValue != newValue)
		//	{
		//		oldValue?.UnBind();
		//		newValue?.Bind();
		//	}
		//}
	}
}
