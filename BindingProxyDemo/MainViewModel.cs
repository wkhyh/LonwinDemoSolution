﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;

namespace BindingProxyDemo
{
	public class MainViewModel : ViewModelBase
	{
		private string _text1;
		private string _text2;

		public string Text1
		{
			get { return _text1; }
			set
			{
				_text1 = value; 
				RaisePropertyChanged();
			}
		}

		public string Text2
		{
			get { return _text2; }
			set
			{
				_text2 = value; 
				RaisePropertyChanged();
			}
		}
	}
}
