﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Sparrow.Chart;

namespace ChartDemo
{
    public class ChartSliderContainer : ContentControl
    {
        public double Value1
        {
            get { return (double)GetValue(Value1Property); }
            set { SetValue(Value1Property, value); }
        }

        public double Value2
        {
            get { return (double)GetValue(Value2Property); }
            set { SetValue(Value2Property, value); }
        }

        public static readonly DependencyProperty Value1Property =
            DependencyProperty.Register("Value1", typeof(double), typeof(ChartSliderContainer), new PropertyMetadata(5d));

        public static readonly DependencyProperty Value2Property =
            DependencyProperty.Register("Value2", typeof(double), typeof(ChartSliderContainer), new PropertyMetadata(10d));

        private SparrowChart _chart;
        private Grid _grid;
        private Slider _slider1;
        private Slider _slider2;

        static ChartSliderContainer()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ChartSliderContainer), new FrameworkPropertyMetadata(typeof(ChartSliderContainer)));
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            _chart = Content as SparrowChart;
            _grid = GetTemplateChild("PART_Grid") as Grid;
            _slider1 = GetTemplateChild("PART_Slider1") as Slider;
            _slider2 = GetTemplateChild("PART_Slider2") as Slider;
            if (_chart == null || _grid == null || _slider1 == null || _slider2 == null)
            {
                return;
            }

            _chart.XAxis.SizeChanged += OnSizeChanged;
            _chart.YAxis.SizeChanged += OnSizeChanged;

            var minValueBinding = new Binding("MinValue") { Source = _chart.XAxis };
            var maxValueBinding = new Binding("MaxValue") { Source = _chart.XAxis };
            BindingOperations.SetBinding(_slider1, RangeBase.MinimumProperty, minValueBinding);
            BindingOperations.SetBinding(_slider1, RangeBase.MaximumProperty, maxValueBinding);
            BindingOperations.SetBinding(_slider2, RangeBase.MinimumProperty, minValueBinding);
            BindingOperations.SetBinding(_slider2, RangeBase.MaximumProperty, maxValueBinding);
        }

        private void OnSizeChanged(object sender, SizeChangedEventArgs e)
        {
            _grid.Margin = new Thickness(_chart.AxisWidth, 0, 0, _chart.AxisHeight);
        }
    }
}
