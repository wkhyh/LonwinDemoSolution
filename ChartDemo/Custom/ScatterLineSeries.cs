﻿using Sparrow.Chart;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace Sparrow.Chart
{
    public class ScatterLineSeries : ScatterSeries
    {
        /// <summary>
        /// Generates the datas.
        /// </summary>
        public override void GenerateDatas()
        {
            ScatterPoints.Clear();
            if (!IsPointsGenerated)
                Parts.Clear();
            Point endPoint = new Point(0, 0);
            Point startPoint = new Point(0, 0);

            if (this.Points != null && this.SeriesContainer != null && this.Points.Count > 0)
            {
                CalculateMinAndMax();
                ChartPoint oldPoint = new ChartPoint() { XValue = double.MinValue, YValue = double.MinValue };
                IntializePoints();
                foreach (ChartPoint point in this.Points)
                {
                    if (CheckValuePoint(oldPoint, point))
                    {
                        Point linePoint = NormalizePoint(new Point(point.XValue, point.YValue));
                        ScatterPoints.Add(linePoint);
                        oldPoint = point;
                    }
                }
                if (this.RenderingMode == RenderingMode.Default)
                {
                    if (!IsPointsGenerated)
                    {
                        for (int i = 0; i < ScatterPoints.Count; i++)
                        {
                            ScatterPart scatterPart = new ScatterPart(ScatterPoints[i]);
                            Binding sizeBinding = new Binding();
                            sizeBinding.Path = new PropertyPath("ScatterSize");
                            sizeBinding.Source = this;
                            scatterPart.SetBinding(ScatterPart.SizeProperty, sizeBinding);
                            SetBindingForStrokeandStrokeThickness(scatterPart);
                            this.Parts.Add(scatterPart);
                        }
                        IsPointsGenerated = true;
                    }
                    else
                    {
                        int i = 0;
                        foreach (ScatterPart part in this.Parts)
                        {
                            part.X1 = ScatterPoints[i].X;
                            part.Y1 = ScatterPoints[i].Y;
                            part.Refresh();
                            i++;
                        }
                    }
                }

            }
            else
            {
                Parts.Clear();
            }

            if (this.SeriesContainer != null)
                this.SeriesContainer.Invalidate();

            IsRefreshed = false;
        }
    }
}
