﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Sparrow.Chart;

namespace ChartDemo
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly List<Point> _dataItemsSource;

        public ObservableCollection<SeriesBase> ItemsSource { get; set; }

        public MainWindow()
        {
            InitializeComponent();

            _dataItemsSource = new List<Point>()
            {
                new Point(1, 2),
                new Point(2, 3),
                new Point(3, 7),
                new Point(4, 5),
                new Point(5, -5),
            };

            //ItemsSource = new ObservableCollection<SeriesBase>
            //{
            //    new LineSeries() {XPath = "X", YPath = "Y", PointsSource = _dataItemsSource}
            //};

            //TestLineSeries.PointsSource = _dataItemsSource;
            //TestColumnSeries.PointsSource = _dataItemsSource;
            TestStepLineSeries.PointsSource = _dataItemsSource;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //SparrowChart1.Series = new SeriesCollection() { new LineSeries() { PointsSource = _dataItemsSource, XPath = "X", YPath = "Y" } };
            SparrowChart1.Series.Add(new LineSeries() { PointsSource = _dataItemsSource, XPath = "X", YPath = "Y" });
        }
    }
}
