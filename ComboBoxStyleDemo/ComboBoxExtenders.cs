﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace ComboBoxStyleDemo
{
    public class ComboBoxExtenders : DependencyObject
    {
        //public static readonly DependencyProperty SelectedItemUnincludedItemsProperty
        //    = DependencyProperty.RegisterAttached("SelectedItemUnincludedItems", typeof(ItemCollection), typeof(ComboBoxExtenders), new UIPropertyMetadata(new ItemCollection(), OnSelectedItemUnincludedItemsChanged));

        //private static void OnSelectedItemUnincludedItemsChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        //{
        //    var listBox = d as ComboBox;
        //}

        //public static ItemCollection GetSelectedItemUnincludedItems(DependencyObject obj)
        //{
        //    return (ItemCollection)obj.GetValue(SelectedItemUnincludedItemsProperty);
        //}

        //public static void SetSelectedItemUnincludedItems(DependencyObject obj, ItemCollection value)
        //{
        //    obj.SetValue(SelectedItemUnincludedItemsProperty, value);
        //}


        public static readonly DependencyProperty HideSelectedItemProperty
           = DependencyProperty.RegisterAttached("HideSelectedItem", typeof(bool), typeof(ComboBoxExtenders), new UIPropertyMetadata(false, OnHideSelectedItemChanged));

        private static void OnHideSelectedItemChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var comboBox = d as ComboBox;
            if (comboBox != null)
            {
                comboBox.SelectionChanged += ComboBox_SelectionChanged;
            }
        }

        private static void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var comboBox = sender as ComboBox;
            if (comboBox != null)
            {
                foreach (ComboBoxItem item in comboBox.Items)
                {
                    item.Visibility = item.IsSelected ? Visibility.Collapsed : Visibility.Visible;
                }
            }
        }

        public static ItemCollection GetHideSelectedItem(DependencyObject obj)
        {
            return (ItemCollection)obj.GetValue(HideSelectedItemProperty);
        }

        public static void SetHideSelectedItem(DependencyObject obj, bool value)
        {
            obj.SetValue(HideSelectedItemProperty, value);
        }
    }
}
