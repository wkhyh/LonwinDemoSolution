﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace DateTimePickerDemo
{
    public class DateTimePicker : Xceed.Wpf.Toolkit.DateTimePicker
    {
        private TextBox _textBox;

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            _textBox = GetTemplateChild("PART_TextBox") as TextBox;
            var calendar = GetTemplateChild("PART_Calendar") as Calendar;
            var buttonToday = GetTemplateChild("PART_ButtonToday") as Button;
            var buttonNow = GetTemplateChild("PART_ButtonNow") as Button;

            if (calendar != null)
            {
                calendar.SelectedDatesChanged += Calendar_SelectedDatesChanged;
            }

            if (buttonToday != null)
            {
                buttonToday.Click += ButtonToday_Click;
            }

            if (buttonNow != null)
            {
                buttonNow.Click += ButtonNow_Click;
            }
        }

        protected override void OnIsOpenChanged(bool oldValue, bool newValue)
        {
            base.OnIsOpenChanged(oldValue, newValue);

            if (newValue)
            {
                DateTime dateTime;
                if (DateTime.TryParse(_textBox.Text, out dateTime))
                {
                    Value = dateTime;
                }
            }
        }

        private void Calendar_SelectedDatesChanged(object sender, SelectionChangedEventArgs e)
        {
            var newValue = (DateTime)e.AddedItems?[0];
            if (Value != null)
            {
                Value = new DateTime(newValue.Year, newValue.Month, newValue.Day, Value.Value.Hour, Value.Value.Minute, Value.Value.Second);
            }
        }

        private void ButtonToday_Click(object sender, RoutedEventArgs e)
        {
            if (Value != null)
            {
                Value = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, Value.Value.Hour, Value.Value.Minute, Value.Value.Second);
            }
        }

        private void ButtonNow_Click(object sender, RoutedEventArgs e)
        {
            Value = DateTime.Now;
        }
    }
}
