﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DateTimePickerDemo
{
    public class MainViewModel
    {
        private DateTime _time;

        public DateTime Time
        {
            get { return _time; }
            set { _time = value; }
        }

        public MainViewModel()
        {
            Time = DateTime.Now;
        }
    }
}
