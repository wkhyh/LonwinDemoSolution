﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DateTimePickerDemo
{
    public class TimePicker : Control
    {
        private const string Datetimeformatstring = "D2";

        #region DependencyProperties
        public DateTime Value
        {
            get { return (DateTime)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register("Value", typeof(DateTime), typeof(TimePicker), new PropertyMetadata(DateTime.Now, OnValuePropertyChanged));

        private static void OnValuePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = d as TimePicker;
            var time = (DateTime)e.NewValue;
            if (control != null)
            {
                control.Hour = time.Hour.ToString(Datetimeformatstring);
                control.Minute = time.Minute.ToString(Datetimeformatstring);
            }
        }

        public string Hour
        {
            get { return (string)GetValue(HourProperty); }
            set { SetValue(HourProperty, value); }
        }

        public static readonly DependencyProperty HourProperty =
            DependencyProperty.Register("Hour", typeof(string), typeof(TimePicker), new PropertyMetadata("00", new PropertyChangedCallback(OnHourPropertyChanged)));

        private static void OnHourPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = d as TimePicker;
            var time = Convert.ToInt32(e.NewValue);
            if (control != null)
            {
                control.Value = control.Value.AddHours(time - control.Value.Hour);
            }
        }

        public string Minute
        {
            get { return (string)GetValue(MinuteProperty); }
            set { SetValue(MinuteProperty, value); }
        }

        public static readonly DependencyProperty MinuteProperty =
            DependencyProperty.Register("Minute", typeof(string), typeof(TimePicker), new PropertyMetadata("00", new PropertyChangedCallback(OnMinutePropertyChanged)));

        private static void OnMinutePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = d as TimePicker;
            var time = Convert.ToInt32(e.NewValue);
            if (control != null)
            {
                control.Value = control.Value.AddMinutes(time - control.Value.Minute);
            }
        }

        public IEnumerable HourItemsSource
        {
            get { return (IEnumerable)GetValue(HourItemsSourceProperty); }
            set { SetValue(HourItemsSourceProperty, value); }
        }

        public static readonly DependencyProperty HourItemsSourceProperty =
            DependencyProperty.Register("HourItemsSource", typeof(IEnumerable), typeof(TimePicker), new PropertyMetadata(null));

        public IEnumerable MinuteItemsSource
        {
            get { return (IEnumerable)GetValue(MinuteItemsSourceProperty); }
            set { SetValue(MinuteItemsSourceProperty, value); }
        }

        public static readonly DependencyProperty MinuteItemsSourceProperty =
            DependencyProperty.Register("MinuteItemsSource", typeof(IEnumerable), typeof(TimePicker), new PropertyMetadata(null));
        #endregion

        static TimePicker()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(TimePicker), new FrameworkPropertyMetadata(typeof(TimePicker)));
        }

        public TimePicker()
        {
            HourItemsSource = GetTimeFormatList(0, 23);
            MinuteItemsSource = GetTimeFormatList(0, 59, 5);
        }

        private List<string> GetTimeFormatList(int min, int max, int interval = 1)
        {
            var list = new List<string>();
            for (int i = min; i <= max; i = i + interval)
            {
                list.Add(i.ToString(Datetimeformatstring));
            }
            return list;
        }
    }
}
