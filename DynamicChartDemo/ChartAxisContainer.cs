﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Sparrow.Chart;

namespace DynamicChartDemo
{
    public class ChartAxisContainer : ContentControl
    {
        public IList XAxisItems
        {
            get { return (IList)GetValue(XAxisItemsProperty); }
            private set { SetValue(XAxisItemsProperty, value); }
        }

        public IList YAxisItems
        {
            get { return (IList)GetValue(YAxisItemsProperty); }
            private set { SetValue(YAxisItemsProperty, value); }
        }

        public static readonly DependencyProperty XAxisItemsProperty =
            DependencyProperty.Register("XAxisItems", typeof(IList), typeof(ChartAxisContainer));

        public static readonly DependencyProperty YAxisItemsProperty =
            DependencyProperty.Register("YAxisItems", typeof(IList), typeof(ChartAxisContainer));

        public bool IsXAxisInverted { get; set; }

        public int XAxisMinValue { get; set; }

        public int XAxisMaxValue { get; set; }

        public int XAxisInterval { get; set; }

        public bool IsYAxisInverted { get; set; }

        public int YAxisMinValue { get; set; }

        public int YAxisMaxValue { get; set; }

        public int YAxisInterval { get; set; }

        private SparrowChart _chart;
        private ItemsControl _xItemsControl;
        private ItemsControl _yItemsControl;

        static ChartAxisContainer()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ChartAxisContainer), new FrameworkPropertyMetadata(typeof(ChartAxisContainer)));
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            _chart = Content as SparrowChart;
            _xItemsControl = GetTemplateChild("PART_XItemsControl") as ItemsControl;
            _yItemsControl = GetTemplateChild("PART_YItemsControl") as ItemsControl;

            _chart.SizeChanged += OnChartSizeChanged;
        }

        private void OnChartSizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (_xItemsControl != null)
            {
                var xAxisItemsCount = (XAxisMaxValue - XAxisMinValue) / XAxisInterval;
                var width = e.NewSize.Width / xAxisItemsCount;
                _xItemsControl.Margin = new Thickness(0 - width / 2, 0, 0 - width / 2, 0);
                var xAxisItems = new List<Label>();
                for (int i = XAxisMinValue; i <= XAxisMaxValue; i += XAxisInterval)
                {
                    var lable = new Label()
                    {
                        Content = IsXAxisInverted ? XAxisMaxValue - i : i,
                        Width = width,
                        HorizontalContentAlignment = HorizontalAlignment.Center,
                        VerticalContentAlignment = VerticalAlignment.Top,
                        Foreground = Foreground,
                        Focusable = false
                    };
                    xAxisItems.Add(lable);
                }
                XAxisItems = xAxisItems;
            }

            if (_yItemsControl != null)
            {
                var yAxisItemsCount = (YAxisMaxValue - YAxisMinValue) / YAxisInterval;
                var height = e.NewSize.Height / yAxisItemsCount;
                _yItemsControl.Margin = new Thickness(0, 0 - height / 2, 0, 0 - height / 2);
                var yAxisItems = new List<Label>();
                for (int i = YAxisMinValue; i <= YAxisMaxValue; i += YAxisInterval)
                {
                    var lable = new Label()
                    {
                        Content = IsYAxisInverted ? i : YAxisMaxValue - i,
                        Height = height,
                        HorizontalContentAlignment = HorizontalAlignment.Left,
                        VerticalContentAlignment = VerticalAlignment.Center,
                        Foreground = Foreground,
                        Focusable = false
                    };
                    yAxisItems.Add(lable);
                }
                YAxisItems = yAxisItems;
            }
        }
    }
}
