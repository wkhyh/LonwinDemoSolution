﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace DynamicChartDemo
{
    public class KeyValueModel<TKey, TValue>
    {
        public TKey Key { get; set; }

        public TValue Value { get; set; }

        public KeyValueModel(TKey key, TValue value)
        {
            Key = key;
            Value = value;
        }
    }

    public class MainViewModel
    {
        private DateTime _time;

        public ObservableCollection<KeyValueModel<DateTime, int>> ItemsSource { get; set; }

        public MainViewModel()
        {
            ItemsSource = new ObservableCollection<KeyValueModel<DateTime, int>>();

            var random = new Random(1000);
            _time = DateTime.Now.AddSeconds(-10);
            for (int i = 0; i < 10 * 1000 / 50; i++)
            {
                ItemsSource.Add(new KeyValueModel<DateTime, int>(_time = _time.AddMilliseconds(50), 0));
            }

            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(0, 0, 0, 0, 50);
            timer.Tick += (s1, e1) =>
            {
                ItemsSource.RemoveAt(0);
                ItemsSource.Add(new KeyValueModel<DateTime, int>(_time = _time.AddMilliseconds(50), random.Next(0, 100)));
            };

            timer.Start();
        }
    }
}
