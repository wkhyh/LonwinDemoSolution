﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GridFillerDemo
{
    public class GridFillerItem : ObservableObject
    {
        private bool _isMouseOver;
        private bool _isFill;

        public int Column { get; set; }

        public int Row { get; set; }

        public bool IsMouseOver
        {
            get
            {
                return _isMouseOver;
            }

            set
            {
                _isMouseOver = value;
                RaisePropertyChanged();
            }
        }

        public bool IsFill
        {
            get
            {
                return _isFill;
            }

            set
            {
                _isFill = value;
                RaisePropertyChanged();
            }
        }

        public GridFillerItem(int column, int row)
        {
            Column = column;
            Row = row;
        }
    }
}
