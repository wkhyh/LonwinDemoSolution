﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Markup;
using System.Windows.Media;

namespace GridFillerDemo
{
    [MarkupExtensionReturnType(typeof(IValueConverter))]
    public class GridFillerItemColorConverterExtension : MarkupExtension, IValueConverter
    {
        private SolidColorBrush _trueBrush;
        private SolidColorBrush _falseBrush;

        public GridFillerItemColorConverterExtension(SolidColorBrush trueBrush, SolidColorBrush falseBrush)
        {
            _trueBrush = trueBrush;
            _falseBrush = falseBrush;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool)value ? _trueBrush : _falseBrush;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
