﻿using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace GridFillerDemo
{
    public class MainViewModel
    {
        public int MaxColumn { get; set; }

        public int MaxRow { get; set; }

        public ObservableCollection<GridFillerItem> ItemsSource { get; set; }

        public MainViewModel()
        {
            MouseMoveCommand = new RelayCommand<GridFillerItem>(OnMouseMoveCommand);

            MaxColumn = 9;
            MaxRow = 9;

            ItemsSource = new ObservableCollection<GridFillerItem>();
            for (int i = 0; i < MaxColumn; i++)
            {
                for (int j = 0; j < MaxRow; j++)
                {
                    ItemsSource.Add(new GridFillerItem(i, j));
                }
            }
        }

        public ICommand MouseMoveCommand { get; set; }

        private void OnMouseMoveCommand(GridFillerItem gridFillerItem)
        {
            //Console.WriteLine(gridFillerItem.Column + "," + gridFillerItem.Row);

            foreach (var item in ItemsSource)
            {
                if(item.Column <= gridFillerItem.Column && item.Row <= gridFillerItem.Row)
                {
                    item.IsFill = true;
                }
                else
                {
                    item.IsFill = false;
                }
            }
        }
    }
}
