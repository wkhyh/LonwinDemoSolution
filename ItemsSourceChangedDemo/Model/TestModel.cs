﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ItemsSourceChangedDemo.Model
{
	public class TestModel
	{
		public int Key { get; set; }

		public string Value { get; set; }

		public TestModel(int key, string value)
		{
			Key = key;
			Value = value;
		}
	}
}
