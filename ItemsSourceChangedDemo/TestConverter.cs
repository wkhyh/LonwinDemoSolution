﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace ItemsSourceChangedDemo
{
	public class TestConverter : IValueConverter
	{
		private object _lastValue;

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			_lastValue = value;
			return value;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value != null)
			{
				_lastValue = value;
				return value;
			}
			else
			{
				return _lastValue;
			}
		}
	}
}
