using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using ItemsSourceChangedDemo.Model;

namespace ItemsSourceChangedDemo.ViewModel
{
	/// <summary>
	/// This class contains properties that the main View can data bind to.
	/// <para>
	/// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
	/// </para>
	/// <para>
	/// You can also use Blend to data bind with the tool's support.
	/// </para>
	/// <para>
	/// See http://www.galasoft.ch/mvvm
	/// </para>
	/// </summary>
	public class MainViewModel : ViewModelBase
	{
		private int _value;

		public int Value
		{
			get { return _value; }
			set
			{
				_value = value;
				RaisePropertyChanged();
			}
		}

		private ObservableCollection<TestModel> _itemsSource;

		public ObservableCollection<TestModel> ItemsSource
		{
			get { return _itemsSource; }
			set
			{
				_itemsSource = value; 
				RaisePropertyChanged();
			}
		}

		public ICommand TestCommand { get; }

		public ICommand TestCommand2 { get; }

		private int _count;

		public MainViewModel()
		{
			_itemsSource = new ObservableCollection<TestModel>()
			{
				new TestModel(1, $"str{_count++}"),
				new TestModel(2, $"str{_count++}")
			};

			Value = 1;

			TestCommand = new RelayCommand(OnTestCommand);
			TestCommand2 = new RelayCommand(OnTestCommand2);
		}

		private void OnTestCommand()
		{
			ItemsSource = new ObservableCollection<TestModel>()
			{
				new TestModel(1, $"str{_count++}"),
				new TestModel(2, $"str{_count++}")
			};
		}

		private void OnTestCommand2()
		{
			Value = Value;
		}
	}
}