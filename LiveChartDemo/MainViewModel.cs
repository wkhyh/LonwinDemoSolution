﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Threading;
using GalaSoft.MvvmLight;
using LiveCharts;
using LiveCharts.Defaults;

namespace LiveChartDemo
{
    public class MainViewModel : ViewModelBase
    {
        private Timer _timer;
        private Random _random;

        private ChartValues<double> _value1;
        private ChartValues<double> _value2;

        public ChartValues<double> Values1
        {
            get { return _value1; }
            set
            {
                _value1 = value; 
                RaisePropertyChanged();
            }
        }

        public ChartValues<double> Values2
        {
            get { return _value2; }
            set
            {
                _value2 = value;
                RaisePropertyChanged();
            }
        }

        public MainViewModel()
        {
            var value1 = new ChartValues<double>();
            var value2 = new ChartValues<double>();

            _random = new Random();
            for (int i = 0; i < 200; i++)
            {
                value1.Add(_random.Next(0, 100));
                value2.Add(10);
            }
            Values1 = value1;
            Values2 = value2;

            _timer = new Timer(50);
            _timer.Elapsed += OnTimerElapsed;
            _timer.Start();
        }

        private void OnTimerElapsed(object sender, ElapsedEventArgs e)
        {
            Dispatcher.CurrentDispatcher.Invoke(() =>
            {
                Values1.Add(_random.Next(0, 100));
                Values1.RemoveAt(0);

                var value2 = new ChartValues<double>();
                var tempValue = _random.Next(0, 100);
                for (int i = 0; i < 200; i++)
                {
                    value2.Add(10);
                }
                Values2 = value2;
            });
        }
    }
}
