﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LoginDemo
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            Test();
        }


        static CookieContainer GetCookie(string postString, string postUrl)
        {
            CookieContainer cookie = new CookieContainer();

            HttpWebRequest httpRequest = (HttpWebRequest)WebRequest.Create(postUrl);//创建http 请求
            httpRequest.CookieContainer = cookie;//设置cookie
            httpRequest.Method = "POST";//POST 提交
            httpRequest.KeepAlive = true;
            httpRequest.UserAgent = "Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko";
            httpRequest.Accept = "text/html, application/xhtml+xml, */*";
            httpRequest.ContentType = "application/x-www-form-urlencoded";//以上信息在监听请求的时候都有的直接复制过来
            byte[] bytes = Encoding.UTF8.GetBytes(postString);
            httpRequest.ContentLength = bytes.Length;
            Stream stream = httpRequest.GetRequestStream();
            stream.Write(bytes, 0, bytes.Length);
            stream.Close();//以上是POST数据的写入
            HttpWebResponse httpResponse = (HttpWebResponse)httpRequest.GetResponse();//获得 服务端响应
            return cookie;//拿到cookie
        }

        static string GetContent(CookieContainer cookie, string url)
        {
            string content;
            HttpWebRequest httpRequest = (HttpWebRequest)WebRequest.Create(url);
            httpRequest.CookieContainer = cookie;
            httpRequest.Referer = url;
            httpRequest.UserAgent = "Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko";
            httpRequest.Accept = "text/html, application/xhtml+xml, */*";
            httpRequest.ContentType = "application/x-www-form-urlencoded";
            httpRequest.Method = "GET";
            HttpWebResponse httpResponse = (HttpWebResponse)httpRequest.GetResponse();
            using (Stream responsestream = httpResponse.GetResponseStream())
            {
                using (StreamReader sr = new StreamReader(responsestream, Encoding.UTF8))
                {
                    content = sr.ReadToEnd();
                }
            }
            return content;
        }

        static string PostContent(CookieContainer cookie, string url, string referer, string postString)
        {
            string content;
            HttpWebRequest httpRequest = (HttpWebRequest)WebRequest.Create(url);
            httpRequest.CookieContainer = cookie;
            httpRequest.Referer = referer;
            httpRequest.UserAgent = "Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko";
            httpRequest.Accept = "text/html, application/xhtml+xml, */*";
            httpRequest.ContentType = "application/x-www-form-urlencoded";
            httpRequest.Method = "POST";
            byte[] bytes = Encoding.UTF8.GetBytes(postString);
            httpRequest.ContentLength = bytes.Length;
            Stream stream = httpRequest.GetRequestStream();
            stream.Write(bytes, 0, bytes.Length);
            stream.Close();//以上是POST数据的写入
            HttpWebResponse httpResponse = (HttpWebResponse)httpRequest.GetResponse();//获得 服务端响应
            using (Stream responsestream = httpResponse.GetResponseStream())
            {
                using (StreamReader sr = new StreamReader(responsestream, Encoding.UTF8))
                {
                    content = sr.ReadToEnd();
                }
            }

            return content;
        }

        private void Test()
        {
            Dictionary<string, string> postParams = new Dictionary<string, string>();
            postParams.Add("code", "57110");
            postParams.Add("userName", "0000034101");
            postParams.Add("password", "123456");

            // 要提交的字符串数据。格式形如:user=uesr1&password=123  
            string postString = "";
            foreach (KeyValuePair<string, string> de in postParams)
            {
                //把提交按钮中的中文字符转换成url格式，以防中文或空格等信息  
                postString += System.Web.HttpUtility.UrlEncode(de.Key.ToString()) + "=" + System.Web.HttpUtility.UrlEncode(de.Value.ToString()) + "&";
            }

            var cookie = GetCookie(postString, @"http://s2.boduogroup.com/web-admin-ui/admin/account/check");
            //var t1 = GetContent(temp, @"http://s2.boduogroup.com/web-admin-ui/admin/index");

            var t2 = GetContent(cookie, @"http://s2.boduogroup.com/web-admin-ui/admin/store/colligate/list?t=0.873756748732369&startTime=2018-01-01%2000:00&endTime=2018-01-29%2023:59");
            var t3 = PostContent(cookie,
                @"http://s2.boduogroup.com/web-admin-ui/admin/store/colligate/list/ajaxData?t=0.06527093399752815",
                @"http://s2.boduogroup.com/web-admin-ui/admin/store/colligate/list?t=0.873756748732369&startTime=2018-01-01%2000:00&endTime=2018-01-29%2023:59",
                @"sEcho=2&iColumns=10&sColumns=%2C%2C%2C%2C%2C%2C%2C%2C%2C&iDisplayStart=0&iDisplayLength=1000&mDataProp_0=&mDataProp_1=day&mDataProp_2=periodOfTime&mDataProp_3=tsCount&mDataProp_4=tsMoney&mDataProp_5=wsCount&mDataProp_6=wsMoney&mDataProp_7=wdCount&mDataProp_8=wdMoney&mDataProp_9=allMoney&storeId=&startTime=2018-01-01+00%3A00&endTime=2018-01-29+23%3A59");

        }
    }
}
