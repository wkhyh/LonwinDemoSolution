﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace MatrixDemo
{
	public class CustomShape : Shape
	{
		protected override Geometry DefiningGeometry { get; }

		public CustomShape()
		{
			DefiningGeometry = new RectangleGeometry
			{
				Rect = new Rect(60, 15, 60, 30)
			};
			Fill = Brushes.Black;
		}

		protected override void OnRender(DrawingContext drawingContext)
		{
			base.OnRender(drawingContext);
			Test(drawingContext);

			

			//var visual = new DrawingVisual();
			//using (var dc = visual.RenderOpen())
			//{
			//	Test(dc);
			//}
		}

		private void Test(DrawingContext drawingContext)
		{
			var text = new FormattedText(
				"abc",
				CultureInfo.GetCultureInfo("en-us"),
				FlowDirection.LeftToRight,
				new Typeface("Calibri"),
				10,
				Brushes.Yellow);

			drawingContext.DrawText(text, new Point(0, 0));
		}
	}
}
