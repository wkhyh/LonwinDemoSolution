﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MatrixDemo
{
	/// <summary>
	/// MainWindow.xaml 的交互逻辑
	/// </summary>
	public partial class MainWindow : Window
	{
		private Shape _shape;
		private Matrix _matrix;
		private MatrixTransform _matrixTransform;
		public MainWindow()
		{
			InitializeComponent();

			//_shape = new Rectangle
			//{
			//	Height = 30,
			//	Width = 60,
			//	Fill = Brushes.Black
			//};

			//_shape = new Path
			//{
			//	Data = new RectangleGeometry
			//	{
			//		Rect = new Rect(60, 15, 60, 30)
			//	},
			//	Fill = Brushes.Black
			//};

			_shape = new CustomShape();

			//var shape1 = new Rectangle
			//{
			//	Height = 30,
			//	Width = 60,
			//	Fill = Brushes.Gray
			//};

			var shape1 = new Line
			{
				X1 = 0,
				Y1 = 0,
				X2 = 60,
				Y2 = 15,
				StrokeThickness = 1,
				Stroke = Brushes.White
			};

			//Canvas.SetTop(shape1, 15);

			_matrix = new Matrix();
			_matrixTransform = new MatrixTransform();
			_shape.RenderTransform = _matrixTransform;
			shape1.RenderTransform = _matrixTransform;
			Canvas1.Children.Add(_shape);
			Canvas1.Children.Add(shape1);

			//Canvas1.RenderTransform = _matrixTransform;
		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			Test();
		}

		private void Test()
		{
			//var rect = new Rect(0, 0, 30, 40);

			var matrix = new Matrix();

			matrix.RotateAt(45, 30, 15);
			matrix.Translate(100, 50);
			//matrix.Scale(2, 2);
			//matrix.ScaleAt(2, 2, 115, 70);

			//rect.Transform(matrix);


			_shape.RenderTransform = new MatrixTransform(matrix);
		}

		private Point _oldPoint;

		private void Canvas1_OnMouseMove(object sender, MouseEventArgs e)
		{
			if (e.LeftButton == MouseButtonState.Pressed)
			{
				var newPoint = e.GetPosition(this);

				var temp = newPoint - _oldPoint;
				_matrix.Translate(temp.X, temp.Y);
				_matrixTransform.Matrix = _matrix;
				//_shape.RenderTransform = new MatrixTransform(_matrix);
			}
			_oldPoint = e.GetPosition(this);
		}

		private void Canvas1_OnMouseWheel(object sender, MouseWheelEventArgs e)
		{
			var ratio = e.Delta > 0 ? 1.1 : 0.9;
			//_matrix.ScaleAt(ratio, ratio, _shape.Width * _matrix.M11 / 2 + _matrix.OffsetX, _shape.Height * _matrix.M11 / 2 + _matrix.OffsetY);
			_matrix.Scale(ratio, ratio);
			_matrixTransform.Matrix = _matrix;
			//_shape.RenderTransform = new MatrixTransform(_matrix);
		}
	}
}
