﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;
using CommonServiceLocator;
using MefDemo.Properties;

namespace MefDemo
{
	/// <summary>
	/// App.xaml 的交互逻辑
	/// </summary>
	public partial class App : Application
	{
		private ComposablePartCatalog _catalog;
		private CompositionContainer _container;
		//private IEnumerable<IModuleController> _modules;

		protected override void OnStartup(StartupEventArgs e)
		{
			base.OnStartup(e);

			//_catalog = new AggregateCatalog();
			////// Load module assemblies as well. See App.config file.
			//foreach (string moduleAssembly in Settings.Default.ModuleAssemblies)
			//{
			//	_catalog.Catalogs.Add(new AssemblyCatalog(moduleAssembly));
			//}
			_catalog = new AssemblyCatalog(Assembly.GetExecutingAssembly());

			_container = new CompositionContainer(_catalog, CompositionOptions.DisableSilentRejection);
			CompositionBatch batch = new CompositionBatch();
			batch.AddExportedValue(_container);
			_container.Compose(batch);

			ServiceLocator.SetLocatorProvider(() => new MefServiceLocatorAdapter(_container));
		}
	}
}
