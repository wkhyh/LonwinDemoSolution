﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CommonServiceLocator;

namespace MefDemo
{
	/// <summary>
	/// MainWindow.xaml 的交互逻辑
	/// </summary>
	public partial class MainWindow : Window
	{
		[Import(typeof(ITestClass))]
		public ITestClass Test { get; set; }

		[Import]
		public ITestClass1 Test1 { get; set; }

		public MainWindow()
		{
			InitializeComponent();

			//var t1 = Test;
			//var t2 = ServiceLocator.Current.GetInstance<ITestClass>();
			var t3 = ServiceLocator.Current.GetInstance<ITestClass1>();
		}
	}
}
