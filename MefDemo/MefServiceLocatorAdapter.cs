﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonServiceLocator;

namespace MefDemo
{
	public class MefServiceLocatorAdapter : ServiceLocatorImplBase
	{
		private readonly CompositionContainer _compositionContainer;

		/// <summary>
		/// Initializes a new instance of the <see cref="T:Prism.Mef.MefServiceLocatorAdapter" /> class.
		/// </summary>
		/// <param name="compositionContainer">The MEF composition container.</param>
		public MefServiceLocatorAdapter(CompositionContainer compositionContainer)
		{
			_compositionContainer = compositionContainer;
		}

		/// <summary>Resolves the instance of the requested service.</summary>
		/// <param name="serviceType">Type of instance requested.</param>
		/// <returns>The requested service instance.</returns>
		protected override IEnumerable<object> DoGetAllInstances(Type serviceType)
		{
			List<object> objectList = new List<object>();
			IEnumerable<Lazy<object, object>> exports = _compositionContainer.GetExports(serviceType, null, null);
			objectList.AddRange(exports.Select(export => export.Value));
			return objectList;
		}

		/// <summary>Resolves all the instances of the requested service.</summary>
		/// <param name="serviceType">Type of service requested.</param>
		/// <param name="key">Name of registered service you want. May be null.</param>
		/// <returns>Sequence of service instance objects.</returns>
		protected override object DoGetInstance(Type serviceType, string key)
		{
			var exports = _compositionContainer.GetExports(serviceType, null, key);
			if (exports.Any())
				return exports.Single().Value;
			throw new CompositionException($"Export not found,type:{serviceType},key:{key}");
		}
	}
}
