﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MefDemo
{
    [Export(typeof(ITestClass)), PartCreationPolicy(CreationPolicy.NonShared)]
	public class TestClass : ITestClass
	{
		public void Test()
		{
			throw new NotImplementedException();
		}
	}

	[Export(typeof(ITestClass1)), PartCreationPolicy(CreationPolicy.NonShared)]
	public class TestClass1 : ITestClass1
	{
		[Import]
		public ITestClass ImportTest { get; set; }

		public ITestClass ImportingConstructorTest { get; }

		[ImportingConstructor]
		public TestClass1(ITestClass testClass)
		{
			ImportingConstructorTest = testClass;
		}
	}
}
