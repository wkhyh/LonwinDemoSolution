﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Animation;

namespace MenuItemTestDemo
{
    public class FocusTrigger : Trigger
    {
        public UIElement TargetElement
        {
            get { return (UIElement)GetValue(TargetElementProperty); }
            set { SetValue(TargetElementProperty, value); }
        }

        public static readonly DependencyProperty TargetElementProperty =
            DependencyProperty.Register("TargetElement", typeof(UIElement), typeof(FocusTrigger), new PropertyMetadata(OnTargetElementChanged));

        private static void OnTargetElementChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            //var trigger = d as FocusTrigger;
            //if (e.OldValue != null)
            //{
            //    trigger.EnterActions.Remove(e.OldValue);
            //}
        }
    }
}