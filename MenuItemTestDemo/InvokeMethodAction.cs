﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Interactivity;

namespace MenuItemTestDemo
{
    public class InvokeMethodAction : TriggerAction<DependencyObject>
    {
        public DependencyObject TargetElement
        {
            get { return (DependencyObject)GetValue(TargetElementProperty); }
            set { SetValue(TargetElementProperty, value); }
        }

        public static readonly DependencyProperty TargetElementProperty =
            DependencyProperty.Register("TargetElement", typeof(DependencyObject), typeof(InvokeMethodAction));

        public string MethodName { get; set; }

        protected override void Invoke(object parameter)
        {
            if (TargetElement != null && !string.IsNullOrWhiteSpace(MethodName))
            {
                TargetElement.GetType().GetMethod(MethodName)?.Invoke(TargetElement, new object[0]);
            }
        }
    }
}
