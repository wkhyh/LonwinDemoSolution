﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MenuStyleDemo
{
    /// <summary>
    /// QuickNavigationWindow.xaml 的交互逻辑
    /// </summary>
    public partial class QuickNavigationWindow : Window
    {
        public List<TestModel> ItemsSource { get; set; }

        public QuickNavigationWindow()
        {
            InitializeComponent();

            ItemsSource = new List<TestModel>
            {
                new TestModel() { Name = "abc" },
                new TestModel() { Name = "abc" },
                new TestModel() { Name = "abc" },
                new TestModel() { Name = "abc" },
                new TestModel() { Name = "abc" },
                new TestModel() { Name = "abc" },
                new TestModel() { Name = "abc" },
                new TestModel() { Name = "abc" },
                new TestModel() { Name = "abc" },
                new TestModel() { Name = "abc" }
            };
        }
    }


    public class TestModel
    {
        public string Name { get; set; }
    }
}
