﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MvvmlightDemo
{
	public class TestCommand : ICommand
	{
		public bool CanExecute(object parameter)
		{
			Console.WriteLine("123");

			//throw new NotImplementedException();
			return true;
		}

		public void Execute(object parameter)
		{
			//throw new NotImplementedException();
		}

		public event EventHandler CanExecuteChanged
		{
			add { CommandManager.RequerySuggested += value; }
			remove { CommandManager.RequerySuggested -= value; }
		}
	}
}
