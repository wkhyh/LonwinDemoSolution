using System;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;

namespace MvvmlightDemo.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
	    private string _text1;

	    public string Text1
	    {
		    get { return _text1; }
		    set
		    {
			    _text1 = value; 
			    RaisePropertyChanged();
		    }
	    }

		public ICommand TestCommand { get; }

	    /// <summary>
		/// Initializes a new instance of the MainViewModel class.
		/// </summary>
		public MainViewModel()
        {
			////if (IsInDesignMode)
			////{
			////    // Code runs in Blend --> create design time data.
			////}
			////else
			////{
			////    // Code runs "for real"
			////}

			//TestCommand = new RelayCommand(OnTestCommand, CanTestCommand);
			TestCommand = new TestCommand();
		}

	    private void OnTestCommand()
	    {
		    
	    }

	    private bool CanTestCommand()
	    {
			Console.WriteLine("------");

		    return true;
	    }
	}
}