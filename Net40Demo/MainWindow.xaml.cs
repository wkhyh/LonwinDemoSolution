﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Net40Demo
{
	/// <summary>
	/// MainWindow.xaml 的交互逻辑
	/// </summary>
	public partial class MainWindow : Window
	{
		private readonly DispatcherTimer _timer;

		public MainWindow()
		{
			InitializeComponent();

			_timer = new DispatcherTimer();
			_timer.Interval = TimeSpan.FromSeconds(1);
			_timer.Tick += Timer_Tick;
		}

		private void Timer_Tick(object sender, EventArgs e)
		{
			if (_visual != null)
			{
				TestPanel.RemoveVisual(_visual);
			}
			Test();
		}

		private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
		{
			Test();
			//_timer.Start();
		}

		private DrawingVisual _visual;
		private void Test()
		{
			var t1 = DateTime.Now;

			var random = new Random();
			_visual = new DrawingVisual();

			DoubleAnimation da = new DoubleAnimation(0, 2, new Duration(TimeSpan.FromMilliseconds(500)));
			ColorAnimation ca = new ColorAnimation(Colors.Transparent, Colors.Red, new Duration(TimeSpan.FromMilliseconds(500)));
			//var clock = da.CreateClock();

			//using (DrawingContext dc = _visual.RenderOpen())
			//{



			//	Pen pen = new Pen(Brushes.Red, 2);
			//	//pen.BeginAnimation(Pen.ThicknessProperty, da);
			//	//pen.Freeze();  //冻结画笔，这样能加快绘图速度
			//	for (int i = 0; i < 5000; i++)
			//	{



			//		dc.DrawLine(pen, new Point(i, random.Next(0, 1000)), new Point(i + 1, random.Next(0, 1000)));
			//	}
			//}
			//TestPanel.AddVisual(_visual);

			var brush = Brushes.Red.Clone();
			Pen pen = new Pen(brush, 2);
			//pen.BeginAnimation(Pen.ThicknessProperty, da);
			//brush.BeginAnimation(SolidColorBrush.ColorProperty, ca);

			pen.Freeze();  //冻结画笔，这样能加快绘图速度

			for (int i = 0; i < 5000; i++)
			{
				_visual = new DrawingVisual();
				using (DrawingContext dc = _visual.RenderOpen())
				{
					dc.DrawLine(pen, new Point(i, random.Next(0, 1000)), new Point(i + 1, random.Next(0, 1000)));
				}
				TestPanel.AddVisual(_visual);
			}

			Console.WriteLine(DateTime.Now - t1);
		}
	}
}
