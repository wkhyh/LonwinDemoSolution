﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Net40Demo
{
	using System.Collections.Generic;
	using System.Windows.Controls;
	using System.Windows.Media;
	using System.Windows;

	namespace PostViewer
	{
		public class TestPanel : Panel
		{
			private List<Visual> visuals = new List<Visual>();

			//获取Visual的个数
			protected override int VisualChildrenCount
			{
				get { return visuals.Count; }
			}

			//获取Visual
			protected override Visual GetVisualChild(int index)
			{
				return visuals[index];
			}

			//添加Visual
			public void AddVisual(Visual visual)
			{
				visuals.Add(visual);

				base.AddVisualChild(visual);
				//base.AddLogicalChild(visual);
			}

			//删除Visual
			public void RemoveVisual(Visual visual)
			{
				visuals.Remove(visual);

				base.RemoveVisualChild(visual);
				//base.RemoveLogicalChild(visual);
			}

			//命中测试
			public DrawingVisual GetVisual(Point point)
			{
				HitTestResult hitResult = VisualTreeHelper.HitTest(this, point);
				return hitResult.VisualHit as DrawingVisual;
			}
		}
	}
}
