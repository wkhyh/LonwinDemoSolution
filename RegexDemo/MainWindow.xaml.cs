﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RegexDemo
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Txt_TextChanged(object sender, TextChangedEventArgs e)
        {
            Console.WriteLine(Regex.IsMatch(Txt.Text, "^[A-Za-z\u4e00-\u9fa5]+$"));
        }

        private void Txt_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            //e.Handled = true;
            Console.WriteLine(e.Text);
        }
    }
}
