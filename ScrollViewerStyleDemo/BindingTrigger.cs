﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ScrollViewerStyleDemo
{
    public class BindingTrigger : Trigger
    {
        public new object Value
        {
            get { return (object)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register("Value", typeof(object), typeof(BindingTrigger), new PropertyMetadata(null));

        public BindingTrigger()
        {
            
        }
    }
}
