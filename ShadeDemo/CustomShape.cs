﻿using System.Globalization;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace ShadeDemo
{
	public class CustomShape : Shape
	{
		protected override Geometry DefiningGeometry { get; }

		public CustomShape()
		{
			DefiningGeometry = new RectangleGeometry
			{
				Rect = new Rect(60, 15, 60, 30)
			};
			Stroke = Brushes.Yellow;
			StrokeThickness = 2;
		}

		protected override void OnRender(DrawingContext drawingContext)
		{
			base.OnRender(drawingContext);

			var borderGeometry = new RectangleGeometry
			{
				Rect = new Rect(0, 0, ActualWidth, ActualHeight)
			};

			var combinedGeometry = new CombinedGeometry(GeometryCombineMode.Exclude, borderGeometry, DefiningGeometry);


			//drawingContext.DrawRectangle(Brushes.Black, new Pen(Brushes.Black, 1d), new Rect(0, 0, ActualWidth, ActualHeight));
			drawingContext.DrawGeometry(Brushes.Black, new Pen(Brushes.Black, 1d), combinedGeometry);
		}
	}
}
