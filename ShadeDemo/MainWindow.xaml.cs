﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ShadeDemo
{
	/// <summary>
	/// MainWindow.xaml 的交互逻辑
	/// </summary>
	public partial class MainWindow : Window
	{
		private Shape _shape;

		public MainWindow()
		{
			InitializeComponent();

			_shape = new CustomShape();

			_shape.SetBinding(Shape.WidthProperty, new Binding("ActualWidth") { Source = Canvas1 });
			_shape.SetBinding(Shape.HeightProperty, new Binding("ActualHeight") { Source = Canvas1 });

			Canvas1.Children.Add(_shape);
		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{

		}
	}
}
