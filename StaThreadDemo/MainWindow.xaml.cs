﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StaThreadDemo
{
	/// <summary>
	/// MainWindow.xaml 的交互逻辑
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();
		}

		private Thread thread;
		private void Button_Click(object sender, RoutedEventArgs e)
		{
			//Test();

			thread = new Thread(() =>
			{
				Test();
				Test();
				Test();
			});
			thread.SetApartmentState(ApartmentState.STA);
			thread.IsBackground = true;
			thread.Start();




			//Thread.Sleep(10000);

			//thread.Abort();
		}

		private void Test()
		{
			//var txt = new TextBox();
			//txt.Text = "abc";
			//TextBox1.Text += "abc";

			var win = new Window
			{
				Width = 400,
				Height = 300,
				Topmost = true,
				//Content = new Button()
				//{
				//	Content = "Button",
				//	Width = 75,
				//	Height = 23,
				//	HorizontalAlignment = HorizontalAlignment.Center,
				//	VerticalAlignment = VerticalAlignment.Center
				//}
			};

			var canvas = new Canvas();
			win.Content = canvas;

			var random = new Random();
			for (int i = 0; i < 10000; i++)
			{
				var shape = new Rectangle
				{
					Width = 5,
					Height = 5,
					Fill = Brushes.Black
				};

				Canvas.SetTop(shape, random.Next(0, 300));
				Canvas.SetLeft(shape, random.Next(0, 400));
				canvas.Children.Add(shape);
			}

			win.ShowDialog();
		}

		private void Button_Click_1(object sender, RoutedEventArgs e)
		{
			//GC.Collect();
			System.Windows.Threading.Dispatcher.FromThread(thread).Invoke(new Action(() => GC.Collect()));
		}
	}
}
