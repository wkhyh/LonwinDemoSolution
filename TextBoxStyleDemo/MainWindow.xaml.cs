﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TextBoxStyleDemo
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            TextCompositionManager.AddPreviewTextInputStartHandler(Txt, new TextCompositionEventHandler(OnPreviewTextInput));
        }

        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            //e.Handled = true;
        }

        private void OnPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            //e.Handled = true;
        }

        private void Txt_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            var txt = sender as TextBox;
            var temp = e.Changes;
        }
    }
}
