﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace UnformattedContainerDemo
{
	public class AdjustableGrid : Grid
	{
		public static readonly DependencyProperty RowCountProperty =
			DependencyProperty.Register("RowCount", typeof(int), typeof(AdjustableGrid), new PropertyMetadata(0, OnRowAndColumnChanged));

		public static readonly DependencyProperty ColumnCountProperty =
			DependencyProperty.Register("ColumnCount", typeof(int), typeof(AdjustableGrid), new PropertyMetadata(0, OnRowAndColumnChanged));

		public int RowCount
		{
			get { return (int)GetValue(RowCountProperty); }
			set { SetValue(RowCountProperty, value); }
		}

		public int ColumnCount
		{
			get { return (int)GetValue(ColumnCountProperty); }
			set { SetValue(ColumnCountProperty, value); }
		}

		public static void OnRowAndColumnChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var grid = d as AdjustableGrid;
			if (grid.RowCount == 0 || grid.ColumnCount == 0)
			{
				return;
			}

			//grid.RowDefinitions.Add(new RowDefinition());
			//grid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(5) });
			//grid.RowDefinitions.Add(new RowDefinition());

			//grid.ColumnDefinitions.Add(new ColumnDefinition());
			//grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(5) });
			//grid.ColumnDefinitions.Add(new ColumnDefinition());

			//grid.Children.Add(new GridSplitter());


		}

		public AdjustableGrid()
		{
			
		}
	}
}
