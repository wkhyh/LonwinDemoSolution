﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UnformattedContainerDemo
{
	public class UnformattedContainer : ContentControl
	{
		public static readonly DependencyProperty RowProperty =
			DependencyProperty.Register("Row", typeof(int), typeof(UnformattedContainer), new PropertyMetadata(0, OnRowAndColumnChanged));

		public static readonly DependencyProperty ColumnProperty =
			DependencyProperty.Register("Column", typeof(int), typeof(UnformattedContainer), new PropertyMetadata(0, OnRowAndColumnChanged));

		public static readonly DependencyProperty ItemsSourceProperty =
			DependencyProperty.Register("ItemsSource", typeof(IEnumerable), typeof(UnformattedContainer), new PropertyMetadata(OnItemsSourceChanged));

		public int Row
		{
			get { return (int)GetValue(RowProperty); }
			set { SetValue(RowProperty, value); }
		}

		public int Column
		{
			get { return (int)GetValue(ColumnProperty); }
			set { SetValue(ColumnProperty, value); }
		}

		public IEnumerable ItemsSource
		{
			get { return (IEnumerable)GetValue(ItemsSourceProperty); }
			set { SetValue(ItemsSourceProperty, value); }
		}

		public static void OnRowAndColumnChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var control = d as UnformattedContainer;
			if (control.Row == 0 || control.Column == 0)
			{
				control.Content = null;
				return;
			}

			var grid = new Grid();
			for (var i = 0; i < control.Row; i++)
			{
				grid.RowDefinitions.Add(new RowDefinition());
				if (i != control.Row - 1)
				{
					grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(5) });

					var gridSplitter = new GridSplitter
					{
						HorizontalAlignment = HorizontalAlignment.Stretch,
						VerticalAlignment = VerticalAlignment.Stretch
					};
					Grid.SetRow(gridSplitter, i * 2 + 1);
					Grid.SetColumnSpan(gridSplitter, control.Column * 2 - 1);
					grid.Children.Add(gridSplitter);
				}
			}

			for (var i = 0; i < control.Column; i++)
			{
				grid.ColumnDefinitions.Add(new ColumnDefinition());
				if (i != control.Column - 1)
				{
					grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(5) });

					var gridSplitter = new GridSplitter
					{
						HorizontalAlignment = HorizontalAlignment.Stretch,
						VerticalAlignment = VerticalAlignment.Stretch
					};
					Grid.SetColumn(gridSplitter, i * 2 + 1);
					Grid.SetRowSpan(gridSplitter, control.Row * 2 - 1);
					grid.Children.Add(gridSplitter);
				}
			}
			control.Content = grid;
		}

		public static void OnItemsSourceChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var control = d as UnformattedContainer;
			if (control.Row == 0 || control.Column == 0)
			{
				throw new ArgumentOutOfRangeException(nameof(ItemsSource), "Must set Row and Column before set ItemsSource.");
			}

			var itemsSource = e.NewValue as IEnumerable;
			if (itemsSource != null)
			{
				var enumerator = itemsSource.GetEnumerator();
				var grid = control.Content as Grid;
				while (enumerator.MoveNext())
				{
					//var uiElement = enumerator.Current as UIElement;
					//if (uiElement != null)
					//{
					//	grid.Children.Add(uiElement);
					//}
				}
			}
		}
	}
}