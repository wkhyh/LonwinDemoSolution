﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Interactivity;

namespace WaterMarkTextBoxStyleDemo
{
    public class SetPropertyAction : TriggerAction<DependencyObject>
    {
        public DependencyObject TargetElement
        {
            get { return (DependencyObject)GetValue(TargetElementProperty); }
            set { SetValue(TargetElementProperty, value); }
        }

        public static readonly DependencyProperty TargetElementProperty =
            DependencyProperty.Register("TargetElement", typeof(DependencyObject), typeof(SetPropertyAction));

        public object Value
        {
            get { return (object)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register("Value", typeof(object), typeof(SetPropertyAction));

        public string PropertyName { get; set; }

        protected override void Invoke(object parameter)
        {
            if (TargetElement != null && !string.IsNullOrWhiteSpace(PropertyName))
            {
                TargetElement.GetType().GetProperty(PropertyName)?.SetValue(TargetElement, Value);
            }
        }
    }
}
