﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Newtonsoft.Json;

namespace WebTestDemo
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public const string AccessToken =
                "bsvODqYPgrh8SzJuRanjmq6KqIlcEc1KreCJxvo5GQ5ylXMa7XH5nUNmqpCKBMsyRXl6ce9DWZF6Pc4cDN1vRTIEoKih2NePMOQQSNyi_DMNZlfGwQ7vaZ323SEtGycNHBZhAEACWU"
            ;

        public const string BaseUrl = "https://api.weixin.qq.com/cgi-bin/";

        public MainWindow()
        {
            InitializeComponent();

            Test();
        }

        public async void Test()
        {
            //var httpClient = new HttpClient();
            //var result = await httpClient.GetAsync(
            //    @"https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=wx779f94a55589e2b8&secret=b830eb82cd65de27c91310b4b1531004");

            //var str = await result.Content.ReadAsStringAsync();
            FileStream fs = new FileStream(@"G:\bd_logo1_31bdc765.png", FileMode.Open);
            byte[] bytes = new byte[fs.Length];
            fs.Read(bytes, 0, (int)fs.Length);
            var temp = UploadMaterialImage(bytes, AccessToken);
        }

        public MaterialImageReturn UploadMaterialImage(byte[] imageFile, string token)
        {
            var apitype = "media";
            var url = string.Concat(BaseUrl, apitype, "/upload", "?access_token=", token, "&type=", "image");
            var boundary = "fbce142e-4e8e-4bf3-826d-cc3cf506cccc";
            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("User-Agent", "KnowledgeCenter");
            client.DefaultRequestHeaders.Remove("Expect");
            client.DefaultRequestHeaders.Remove("Connection");
            client.DefaultRequestHeaders.ExpectContinue = false;
            client.DefaultRequestHeaders.ConnectionClose = true;
            var content = new MultipartFormDataContent(boundary);
            content.Headers.Remove("Content-Type");
            content.Headers.TryAddWithoutValidation("Content-Type", "multipart/form-data; boundary=" + boundary);
            var contentByte = new ByteArrayContent(imageFile);
            content.Add(contentByte);
            contentByte.Headers.Remove("Content-Disposition");
            contentByte.Headers.TryAddWithoutValidation("Content-Disposition", "form-data; name=\"media\";filename=\"111.jpg\"" + "");
            contentByte.Headers.Remove("Content-Type");
            contentByte.Headers.TryAddWithoutValidation("Content-Type", "image/jpg");
            try
            {
                var result = client.PostAsync(url, content);
                if (result.Result.StatusCode != HttpStatusCode.OK)
                    throw new Exception(result.Result.Content.ReadAsStringAsync().Result);


                var temp = result.Result.Content.ReadAsStringAsync().Result;

                if (result.Result.Content.ReadAsStringAsync().Result.Contains("media_id"))
                {
                    var resultContent = result.Result.Content.ReadAsStringAsync().Result;
                    var materialEntity = JsonConvert.DeserializeObject<MaterialImageReturn>(resultContent);
                    return materialEntity;
                }
                throw new Exception(result.Result.Content.ReadAsStringAsync().Result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + ex.InnerException.Message);
            }
        }
    }

    public class MaterialImageReturn
    {
        /// <summary>
        /// 媒体文件类型，分别有图片（image）、语音（voice）、视频（video）和缩略图（thumb，主要用于视频与音乐格式的缩略图）
        /// </summary>
        public string type { get; set; }

        /// <summary>
        /// 媒体文件上传后，获取时的唯一标识
        /// </summary>
        public string media_id { get; set; }

        /// <summary>
        /// 媒体文件上传时间戳
        /// </summary>
        public long created_at { get; set; }
    }
}
